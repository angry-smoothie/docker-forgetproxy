FROM ubuntu:16.04

HEALTHCHECK --interval=120s --timeout=10s --retries=3 \
  CMD /usr/bin/healthcheck
VOLUME ["/var/log", "/var/cache/squid"]
ENTRYPOINT ["/usr/bin/noproxy"]

RUN userdel proxy && \
    groupadd -g 10000 proxy && useradd -u 10000 -g proxy proxy && \
    apt-get update && \
    apt-get -y install \
    curl \
    procps \
    psmisc \
    redsocks \
    iptables \
    squid3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Squid
RUN sed -i "s/^#acl localnet/acl localnet/" /etc/squid/squid.conf && \
 sed -i "s/^#http_access allow localnet/http_access allow localnet/" /etc/squid/squid.conf && \
 sed -i "s/^http_access deny !Safe_ports/#http_access deny !Safe_ports/" /etc/squid/squid.conf && \
 sed -i "s/^http_access deny CONNECT !SSL_ports/#http_access deny CONNECT !SSL_ports/" /etc/squid/squid.conf && \
 echo "forwarded_for delete" >> /etc/squid/squid.conf && \
 mkdir -p /var/cache/squid && \
 chown -R proxy:proxy /var/cache/squid

COPY bin/ /usr/bin/
COPY etc/* /etc/
